"use strict";
const EventEmitter = require('events');

class ChatApp extends EventEmitter {
  /**
   * @param {String} title
   */

  constructor(title) {
    super();

    this.title = title;

    // Часть 2, пункт 1
    let close = () => {
      this.emit("close")
    };

    // Посылать каждую секунду сообщение
    setInterval(() => {
      this.emit('message', `${this.title}: ping-pong`);
  }, 1000);
  }
}

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');

// Часть 1, пункт 2
vkChat.setMaxListeners(2);

let chatOnMessage = (message) => {
  console.log(message);
};

// Часть 1, пункты 1 и 3
let getReadyOnMessage = () => {
  console.log("Готовлюсь к ответу");
};

// Часть 2, пункт 2
let closeVkChat = () => {
  console.log("Чат вконтакте закрылся :(");
};

webinarChat.on("message", getReadyOnMessage);
vkChat.on("message", getReadyOnMessage);
webinarChat.on('message', chatOnMessage);
facebookChat.on('message', chatOnMessage);
vkChat.on('message', chatOnMessage);
vkChat.on("close", closeVkChat); // Часть 2, пункт 3

// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
  vkChat.removeListener('message', chatOnMessage);
  vkChat.removeListener("message", getReadyOnMessage);
  vkChat.emit("close");
}, 10000 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
  facebookChat.removeListener('message', chatOnMessage);
}, 15000 );

// Дополнительное задание
setTimeout( () => {
  console.log("Закрываю чат вебинара");
  webinarChat.removeAllListeners("message");
}, 30000);